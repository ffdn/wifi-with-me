# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contribmap', '0011_auto_20160316_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contrib',
            name='contrib_type',
            field=models.CharField(default=None, verbose_name='Type de contribution', choices=[('connect', 'Me raccorder au réseau expérimental'), ('share', 'Partager une partie de ma connexion')], max_length=10),
        ),
        migrations.AlterField(
            model_name='contrib',
            name='latitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contrib',
            name='longitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contrib',
            name='status',
            field=models.CharField(null=True, blank=True, default='TOSTUDY', choices=[('TOSTUDY', 'à étudier'), ('TOCONNECT', 'à connecter'), ('CONNECTED', 'connecté'), ('WONTCONNECT', 'pas connectable')], max_length=250),
        ),
    ]
