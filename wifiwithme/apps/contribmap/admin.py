# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib import admin, messages
from django.http import HttpResponseNotAllowed
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, render
from django.template.loader import get_template
from django.template import engines as template_engines
from django.utils.html import format_html

from .emails import send_contributor_email
from .forms import ReminderForm
from .tokens import ContribTokenManager
from .models import Contrib


django_templates = template_engines['django']


# Kinda hackish to do that here
admin.site.site_header = "Administration − Wifi with me"
admin.site.site_title = "Wifi with me"


def send_reminder_action(modeladmin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    return redirect(reverse(
        "admin:send_contrib_reminder",
        args=[",".join(selected)],
    ))
send_reminder_action.short_description = "Renvoyer le lien de gestion par mail"


@admin.register(Contrib)
class ContribAdmin(admin.ModelAdmin):
    search_fields = ["name", "email", "phone"]
    list_display = ("name", "date", "phone", "email", "expired_string")

    readonly_fields = ['date', 'expiration_date']
    fieldsets = [
        [None, {
            'fields': [
                ('name', 'contrib_type'),
                'comment', 'email', 'phone',
                ('date', 'expiration_date'),
            ],
        }],
        ['Localisation', {
            'fields': [
                ('latitude', 'longitude'),
                ('floor', 'floor_total'),
                'orientations', 'roof']
        }],
        ['Partage de connexion', {
            'fields': ['access_type'],
            'classes': ['collapse'],
        }],
        ['Vie privée', {
            'fields': [
                'privacy_name', 'privacy_email', 'privacy_coordinates',
                'privacy_place_details', 'privacy_comment'
            ],
            'classes': ['collapse'],
        }]
    ]

    actions = [send_reminder_action]

    def expired_string(self, obj):
        if obj.is_expired():
            return format_html('<strong style="color: red; cursor: help;" title="Cette entrée excède la durée de rétention et aurait dû être supprimée automatiquement.">expiré</strong>')
        else:
            return 'non expiré'

    expired_string.short_description = 'Expiration'

    def send_reminder(self, request, pks):
        contribs = Contrib.objects.filter(pk__in=pks.split(','))
        contribs_with_email = contribs.exclude(email='')
        contribs_without_email = contribs.filter(email='')

        if request.method not in ('GET', 'POST'):
            return HttpResponseNotAllowed()

        if request.method == 'POST':
            form = ReminderForm(request.POST)
            if form.is_valid():
                for contrib in contribs:
                    send_contributor_email(
                        contrib,
                        django_templates.from_string(form.cleaned_data['mail_subject']),
                        django_templates.from_string(form.cleaned_data['mail_body']),
                        ContribTokenManager().mk_token(contrib),
                        request,
                    )

                messages.add_message(
                    request, messages.INFO,
                    'Mail de rappel bien envoyé à {} destinataires.'.format(
                        contribs_with_email.count()
                    )
                )
                return redirect('admin:contribmap_contrib_changelist')

        elif request.method == 'GET':
            form = ReminderForm(initial={
                'mail_subject': get_template(
                    'contribmap/mails/link_reminder.subject'
                ).template.source,
                'mail_body': get_template(
                    'contribmap/mails/link_reminder.txt'
                ).template.source,
            })

        if contribs_with_email.exists():
            if contribs_without_email.exists():

                messages.add_message(
                    request, messages.WARNING,
                    "Certaines contributions ne mentionnent"
                    + " pas d'email : {}.".format(
                        ', '.join(['« {} »'.format(i) for i in contribs_without_email]))
                    + " Astuce : il doit y avoir un n° de téléphone …"
                )

            # case of form display or re-display (errors)
            return render(
                request, 'admin/contribmap/contrib/send_reminder.html', {
                    'contribs': contribs_with_email,
                    'form': form,
                }
            )
        else:
            messages.add_message(
                request, messages.ERROR,
                "Aucune des contributions sélectionnées ne mentionne une adresse"
                " mail ; impossible d'envoyer un email de rappel."
            )
            return redirect('admin:contribmap_contrib_changelist')

    def get_urls(self):
        return super().get_urls() + [
            url(
                r'^remind/(?P<pks>(\d+,?)+)',
                self.admin_site.admin_view(self.send_reminder),
                name='send_contrib_reminder',
            ),
        ]
