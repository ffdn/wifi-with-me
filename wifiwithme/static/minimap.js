$( document ).ready(function() {
    // Create map
    var lat = parseFloat($('#map').data("lat"));
    var lon = parseFloat($('#map').data("lon"));

    var map = L.map('map', {scrollWheelZoom: false}).setView([lat, lon], 15);
;
    L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="//openstreetmap.org">OpenStreetMap</a> contributors, <a href="//creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="//mapbox.com">Mapbox</a>',
        maxZoom: 18
    }).addTo(map);

  var marker = L.marker([lat, lon]).addTo(map);
});
