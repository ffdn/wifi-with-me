from .base import *

DEBUG = True
INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE_CLASSES += [
    'debug_toolbar.middleware.DebugToolbarMiddleware'
]

try:
    from .local import *
except ImportError:
    pass

# Send emails to console
NOTIFICATION_EMAILS = ['moderator@example.com']
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
STATIC_URL = '/{}assets/'.format(URL_PREFIX)
